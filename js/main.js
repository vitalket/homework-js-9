/* 
   1. Cтворити новий HTML тег на сторінці можливо за допомогою команди createElement("тег")

   2. перший параметр функції insertAdjacentHTML це позиція доданого елемента відносно батьківського елемента.
      Варіанти: "beforebegin" - перед самим елементом, "afterbegin" - всередині елемента, на початку, 
      "beforeend" - всередині елемента, в кінці, "afterend - після самим елементом"

   3. Видалити елемент зі сторінки можна за допомогою команди removeChild або remove()

 */

const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];


function arrList(arr, parent = document.body) {
   const ul = document.createElement("ul");

   arr.forEach(item => {
      const li = document.createElement("li");

      if (Array.isArray(item)) {
         arrList(item, li);
      } else {
         li.textContent = item;
      }
      
      ul.appendChild(li);
   });

   parent.appendChild(ul)
}

arrList(arr);


















